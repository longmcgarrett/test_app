import 'dart:io';
import 'dart:typed_data';
import 'package:audioplayer/audioplayer.dart';
import 'package:flutter/services.dart';
import 'package:path_provider/path_provider.dart';

AudioPlayer audioPlayer = new AudioPlayer();

class AudioManager {

  static final AudioManager _singleton = AudioManager._internal();

  factory AudioManager() {
    return _singleton;
  }

  AudioManager._internal();

  var _buttonSoundPath;
  var _rightSoundPath;
  var _wrongSoundPath;

  Future<ByteData> loadAsset(String sound) async {
    return await rootBundle.load('assets/$sound');
  }

  void preloadAssets() async {
    final buttonFile = new File('${(await getTemporaryDirectory()).path}/select.mp3');
    await buttonFile.writeAsBytes((await loadAsset('select.mp3')).buffer.asUint8List());
    _buttonSoundPath = buttonFile.path;
    final rightFile = new File('${(await getTemporaryDirectory()).path}/right.mp3');
    await rightFile.writeAsBytes((await loadAsset('right.mp3')).buffer.asUint8List());
    _rightSoundPath = rightFile.path;
    final wrongFile = new File('${(await getTemporaryDirectory()).path}/wrong.mp3');
    await wrongFile.writeAsBytes((await loadAsset('wrong.mp3')).buffer.asUint8List());
    _wrongSoundPath = wrongFile.path;
  }

  Future<void> playButtonSelect() async {
    await audioPlayer.play(_buttonSoundPath, isLocal: true);
  }

  Future<void> playSoundRight() async {
    await audioPlayer.play(_rightSoundPath, isLocal: true);
  }

  Future<void> playSoundWrong() async {
    await audioPlayer.play(_wrongSoundPath, isLocal: true);
  }

}