import 'package:firebase_admob/firebase_admob.dart';

MobileAdTargetingInfo _targetingInfo = MobileAdTargetingInfo(
  keywords: <String>['fun', 'beautiful apps', 'games', 'puzzle'],
  childDirected: false,
  testDevices: <String>[], // Android emulators are considered test devices
);

class AdvertisementManager {
  final String appId = '';

  BannerAd myBanner = BannerAd(
    // Replace the testAdUnitId with an ad unit id from the AdMob dash.
    // https://developers.google.com/admob/android/test-ads
    // https://developers.google.com/admob/ios/test-ads
    adUnitId: BannerAd.testAdUnitId,
    size: AdSize.smartBanner,
    targetingInfo: _targetingInfo,
    listener: (MobileAdEvent event) {
      print("BannerAd event is $event");
    },
  );

  InterstitialAd myInterstitial = InterstitialAd(
    // Replace the testAdUnitId with an ad unit id from the AdMob dash.
    // https://developers.google.com/admob/android/test-ads
    // https://developers.google.com/admob/ios/test-ads
    adUnitId: InterstitialAd.testAdUnitId,
    targetingInfo: _targetingInfo,
    listener: (MobileAdEvent event) {
      print("InterstitialAd event is $event");
    },
  );

  static final AdvertisementManager _singleton =
      AdvertisementManager._internal();

  factory AdvertisementManager() {
    return _singleton;
  }

  AdvertisementManager._internal();

  void config() {
    FirebaseAdMob.instance.initialize(appId: appId);
  }
}
