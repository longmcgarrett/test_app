import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';


import 'package:test_app/containers/finalscore.dart';
import 'package:test_app/ultilities/audioPlayer.dart';

import 'package:test_app/models/question.dart';
import 'package:test_app/models/score.dart';

import 'package:test_app/presentations/testscore.dart';
import 'package:test_app/presentations/testquestion.dart';
import 'package:test_app/presentations/testanswer.dart';

enum GameState { wrong, success, none }

class GameScene extends StatefulWidget {
  final int currentLevel;
  final List<Question> listQuestion;
  GameScene({Key key, this.listQuestion, this.currentLevel}) : super(key: key);
  @override
  _GameSceneState createState() => _GameSceneState();
}

class _GameSceneState extends State<GameScene> {
  int currentIndex = 0;
  int currentScore = 0;
  final int additionalScore = 10;
  final int maxQuestion = 20;
  Question get currentQuestion {
    return widget.listQuestion[currentIndex];
  }

  GameState gameState = GameState.none;
  _onPressed(int index) {
    if (gameState != GameState.none) {
      return;
    }
    if (index == currentQuestion.answer) {
      setState(() {
        AudioManager().playSoundRight();
        currentScore += additionalScore;
        gameState = GameState.success;
      });
    } else {
      setState(() {
        AudioManager().playSoundWrong();
        gameState = GameState.wrong;
      });
    }
    _nextQuestion();
  }

  _nextQuestion() {
    Future.delayed(Duration(seconds: 1), () async {
      print("navigate to new scene");
      if (currentIndex == maxQuestion - 1) {
        SharedPreferences pref = await SharedPreferences.getInstance();
        pref.setInt("${widget.currentLevel}", (currentScore.toDouble()/(maxQuestion.toDouble()*additionalScore.toDouble())*100).toInt());
        await Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => FinalScore(finalScore: currentScore,)),
        );
        setState(() {
          currentIndex = 0;
          currentScore = 0;
          gameState = GameState.none;
        });
      } else {
        setState(() {
          gameState = GameState.none;
          currentIndex++;
        });
      }
    });
  }

  Widget get gameStateWidget {
    final width = MediaQuery.of(context).size.width;
    switch (gameState) {
      case GameState.success:
        return Center(
          child: Image.asset(
            'assets/right.png',
            width: width,
            fit: BoxFit.fitHeight,
          ),
        );
      case GameState.wrong:
        return Center(
          child: Image.asset(
            'assets/wrong.png',
            width: width,
            fit: BoxFit.fitHeight,
          ),
        );
      case GameState.none:
    }
    return Container();
  }

  @override
  Widget build(BuildContext context) {
    final width = MediaQuery.of(context).size.width;
    final height = MediaQuery.of(context).size.height;
    return Scaffold(
      body: Stack(
        children: <Widget>[
          Image.asset(
            'assets/bg.png',
            width: width,
            height: height,
            fit: BoxFit.fill,
          ),
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              TestScore(
                score: Score(testScore: currentScore),
              ),
              TestQuestion(
                title: currentQuestion.testquestion,
              ),
              Expanded(
                flex: 1,
                child: Column(
                  children: <Widget>[
                    Expanded(
                        flex: 1,
                        child: TestAnswer(
                          title: currentQuestion.options[0],
                          onPressed: () => _onPressed(0),
                        )),
                    Expanded(
                        flex: 1,
                        child: TestAnswer(
                          title: currentQuestion.options[1],
                          onPressed: () => _onPressed(1),
                        )),
                    Expanded(
                        flex: 1,
                        child: TestAnswer(
                          title: currentQuestion.options[2],
                          onPressed: () => _onPressed(2),
                        )),
                    Expanded(
                        flex: 1,
                        child: TestAnswer(
                          title: currentQuestion.options[3],
                          onPressed: () => _onPressed(3),
                        )),
                  ],
                ),
              )
            ],
          ),
          gameStateWidget
        ],
      ),
    );
  }
}
