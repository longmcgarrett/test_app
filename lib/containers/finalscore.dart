import 'package:flutter/material.dart';

import 'package:test_app/containers/home.dart';

import 'package:test_app/models/score.dart';
import 'package:test_app/ultilities/audioPlayer.dart';

import 'package:test_app/presentations/button.dart';
import 'package:test_app/presentations/finaltestscore.dart';

class FinalScore extends StatefulWidget {
  final int finalScore;
  FinalScore({Key key, this.finalScore}) : super(key: key);

  @override
  _FinalScoreState createState() => _FinalScoreState();
}

class _FinalScoreState extends State<FinalScore> {
  @override
  Widget build(BuildContext context) {
    final width = MediaQuery.of(context).size.width;
    final height = MediaQuery.of(context).size.height;
    return Scaffold(
      body: Stack(
      children: <Widget>[
        Image.asset(
          'assets/finalscore.png',
          width: width,
          height: height,
          fit: BoxFit.fill,
        ),
        Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            FinalTestScore(
              score: Score(testScore: widget.finalScore),
            ),
            Column(
              children: <Widget>[
                GestureDetector(
                  child: LevelButton(
                    text: "Chơi lại",
                  ),
                  onTap: () {
                    AudioManager().playButtonSelect();
                    Navigator.pop(context);
                  }
                ),
                GestureDetector(
                  child: LevelButton(
                    text: "Kết thúc",
                  ),
                  onTap: () {
                    AudioManager().playButtonSelect();
                    Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => Home())
                  );
                }
                )
              ],
            )
          ],
        )
      ],
    ));
  }
}