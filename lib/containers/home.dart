import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:test_app/containers/gameScene.dart';
import 'package:json_utilities/json_utilities.dart';
import 'package:test_app/models/level.dart';
import 'package:test_app/models/question.dart';
import 'package:test_app/ultilities/audioPlayer.dart';

import 'package:test_app/presentations/button.dart';

class Home extends StatefulWidget {
  Home({Key key}) : super(key: key);

  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  List<Question> listQuestion = [];
  List<Level> listLevel = [Level(level: "Level 1"), Level(level: "Level 2"), Level(level: "Level 3"), Level(level: "Level 4"), Level(level: "Level 5")];
  _loadJson(context) async {
    String data =
        await DefaultAssetBundle.of(context).loadString("assets/question.json");
    try {
      List<dynamic> jsonResult = json.decode(data);
      final answers = JSONUtils().get(jsonResult[0], '__text', null).split(',');
      final sentence1 =
          JSONUtils().get(jsonResult[2], '__text', null).split(',');
      final sentence2 =
          JSONUtils().get(jsonResult[3], '__text', null).split(',');
      final sentence3 =
          JSONUtils().get(jsonResult[4], '__text', null).split(',');
      final sentence4 =
          JSONUtils().get(jsonResult[5], '__text', null).split(',');

      JSONUtils()
          .get(jsonResult[1], '__text', null)
          .split('?,')
          .asMap()
          .forEach((index, value) {
        List<String> options = [
          sentence1[index],
          sentence2[index],
          sentence3[index],
          sentence4[index]
        ];
        final answer = int.parse(answers[index]) - 1;
        final answerText = options[answer];
        listQuestion.add(Question(
            answer: answer,
            answerText: answerText,
            testquestion: value + "?",
            options: options));
      });
    } catch (e) {
      print(e);
    }
  }

  @override
  void initState() {
    Future.delayed(Duration(milliseconds: 100), () {
      _loadJson(context);
      getLevelTitle();
    });
    super.initState();
  }

  void onTap(index) async {
    final maxLevel = 5;
    final maxQuestions = listQuestion.length / maxLevel; 
    final currentQuestionLevel = maxQuestions*(index+1);
    final splitedQuestions = listQuestion.skip(currentQuestionLevel.toInt() - maxQuestions.toInt()).take(maxQuestions.toInt()).toList();
    AudioManager().playButtonSelect();
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => GameScene(listQuestion: splitedQuestions, currentLevel: index,)),
    );
  }

  void getLevelTitle() async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    listLevel.asMap().forEach((k,v) {
      var progress = pref.getInt('$k');
      v.progress = progress == null ? 0 : progress ;
    });
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    final width = MediaQuery.of(context).size.width;
    final height = MediaQuery.of(context).size.height;
    return Scaffold(
        body: Stack(
      children: <Widget>[
        Image.asset(
          'assets/pick_a_quiz.png',
          width: width,
          height: height,
          fit: BoxFit.fill,
        ),
        Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: listLevel.asMap().map((key,value) {
            return MapEntry(key,  GestureDetector(
              child: LevelButton(
                text: "${value.level} - ${value.progress}%",
              ),
              onTap:(){
                onTap(key);
              },
            ));
          }).values.toList()
        )
      ],
    ));
  }
}
