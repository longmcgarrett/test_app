import 'package:flutter/material.dart';
import 'package:test_app/models/level.dart';

class LevelButton extends StatelessWidget {
  final String text;
  const LevelButton({Key key, this.text}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(top: 10, bottom: 10),
      width: MediaQuery.of(context).size.width,
      height: 80,
      child: Stack(
        children: <Widget>[
          Image.asset(
            'assets/answerbar.png',
            fit: BoxFit.fill,
            width: MediaQuery.of(context).size.width,
          ),
          Center(
            child: Text(
              text,
              textAlign: TextAlign.center,
              style: TextStyle(color: Colors.white, fontSize: 20),
            ),
          )
        ],
      ),
    );
  }
}
