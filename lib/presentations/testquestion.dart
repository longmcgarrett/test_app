import 'package:flutter/material.dart';

class TestQuestion extends StatelessWidget {
  final String title;
  const TestQuestion({Key key, this.title}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(top: 0, bottom: 10),
      height: 250,
      child: Stack(
        children: <Widget>[
          Image.asset(
            'assets/questionwindow.png',
            fit: BoxFit.fill,
            width: MediaQuery.of(context).size.width,
            height: 250,
          ),
          Center(
              child: Padding(
            padding: EdgeInsets.only(right: 18, left: 18),
            child: Text(
              "$title",
              textAlign: TextAlign.center,
              style: TextStyle(color: Colors.green[900], fontSize: 18),
            ),
          ))
        ],
      ),
    );
  }
}
