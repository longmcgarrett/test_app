import 'package:flutter/material.dart';
import 'package:test_app/models/score.dart';

class FinalTestScore extends StatelessWidget {
  final Score score;
  const FinalTestScore({Key key, this.score}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(top: 10, bottom: 10),
      width: 200,
      height: 50,
      child: Stack(
        children: <Widget>[
          Center(
            child: Image.asset('assets/score.png', fit: BoxFit.fill ,),
          ),
          Positioned.fill(
            top: 7,
            right: -73,
            child: Text(
            "${score.testScore}",
            textAlign: TextAlign.center,
            style: TextStyle(color: Colors.white, fontSize: 13),
          ),
          )
        ],
      )
    );
  }
}