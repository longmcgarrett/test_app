import 'package:flutter/material.dart';

class TestAnswer extends StatelessWidget {
  final String title;
  final Function onPressed;
  const TestAnswer({Key key, this.title, this.onPressed}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: this.onPressed,
      child: Container(
      padding: EdgeInsets.only(top: 10, bottom: 10),
      child: Stack(
        children: <Widget>[
          Image.asset('assets/answerbar.png', fit: BoxFit.fill, width: MediaQuery.of(context).size.width,),
          Center(
            child: Text(
            "$title",
            textAlign: TextAlign.center,
            style: TextStyle(color: Colors.green, fontSize: 15),
          ),
          )
        ],
      ),
    ) ,
    );
  }
}