import 'package:flutter/material.dart';
import 'package:test_app/models/score.dart';

class TestScore extends StatelessWidget {
  final Score score;
  const TestScore({Key key, this.score}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(top: 10, bottom: 10),
      width: 200,
      height: 50,
      child: Stack(
        children: <Widget>[
          Center(
            child: Image.asset('assets/scoreboard.png', fit: BoxFit.fill ,),
          ),
          Center(
            child: Text(
            "${score.testScore}",
            textAlign: TextAlign.center,
            style: TextStyle(color: Colors.white, fontSize: 13),
          ),
          )
        ],
      )
    );
  }
}