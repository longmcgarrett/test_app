class Question {
  String testquestion;
  List<String> options;
  int answer;
  String answerText;

  Question({this.testquestion, this.options, this.answer, this.answerText});
}