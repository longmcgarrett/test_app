import 'package:flutter/material.dart';
import 'package:test_app/ultilities/audioPlayer.dart';
import 'containers/home.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    AudioManager().preloadAssets();
    return MaterialApp(
      title: 'Startup Name Generator',            
      home: Home(),
      theme: ThemeData(

        primarySwatch: Colors.blue,
      ),
    );
  }
}

